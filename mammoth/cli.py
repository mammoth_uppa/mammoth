#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This mammoth sub-module gathers all functions in order to provide a command
line interface.
"""

import os
import argparse

from .mammoth import Mammoth, MammothControls

__version__ = "0.1.0"
__authors__ = "Hugo Santos-Silva, Germain Vallverdu"


TITLE = r"""
   __  __   _   __  __ __  __  ___ _____ _  _
  |  \/  | /_\ |  \/  |  \/  |/ _ \_   _| || |
  | |\/| |/ _ \| |\/| | |\/| | (_) || | | __ |
  |_|  |_/_/ \_\_|  |_|_|  |_|\___/ |_| |_||_|

                                                o-O
                    _.---____..---.            o---O
               __.-"     `  "';     `.         O---o
            .-"               ;      _`.        O-o
          .'                  `.    '-` ;        O
         /                           _  :       o-O
    ,.--'                         _ : ; j      o---O
    `""-.                     :  / \ \ \       O---o
        L                J     ;'   : `.`.._._  O-o
         J    .'""-.     '     J    | | `-._.-`  O
         ;   ;     ]   .'_`.  /     : ;         o-O
        .J  /      :   | ;  \ `.  /L'/         o---O
        F' `.      :   ;_`,  ;  \ '-'          O---o
        !____}     !____}_/  !___}              O-o
"""


def exist(filename):
    """ 'Type' for argparse - checks that file exists but does not open """
    if not os.path.isfile(filename):
        raise argparse.ArgumentTypeError("Input file %s does not exist" % filename)
    return filename


def get_options():
    """ Define command line interface options """

    parser = argparse.ArgumentParser(
        prog="Mammoth",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=TITLE + __doc__)

    parser.add_argument("-i", "--input",
                        help="path to the Mammoth input file",
                        metavar="input.mmt",
                        default="input.mmt",
                        type=exist)
    parser.add_argument("-v", "--verbose", dest='verbose', default=False,
                        action='store_true',
                        help="Verbose mode")

    return parser.parse_args()


def main():
    """ main mammoth function for the command line interface """

    args = get_options()

    print(TITLE)

    # read in input file and do all checks on input parameters
    controls = MammothControls.from_file(args.input)

    # set up the mammoth object and run the main function
    # in the future, select the right Mammoth method depending on the desired calculation
    mammoth_run = Mammoth(controls=controls)
    exit(mammoth_run.main())


if __name__ == '__main__':
    main()
