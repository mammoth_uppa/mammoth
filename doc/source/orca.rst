***********
orca module
***********

The aim of the orca module is to provide several class in order to read or write
ORCA output or input files.

.. automodule:: orca

Orca input file
---------------

.. autoclass:: orca.OrcaInput
    :members:

Orca Hessian file
-----------------

.. autoclass:: orca.OrcaHessian
    :members:

Orca output file
----------------

.. autoclass:: orca.OrcaOutfile
    :members:

Orca VPT2 file
----------------

.. autoclass:: orca.OrcaVPT2
    :members:
