#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions that compute a partial energy contribution to reproduce the stability of strong C-C triple bonds in ReaxFF
Reference for equation numbers: Chenoweth, K., van Duin, A.C.T., Goddard, W.A., 2008. ReaxFF Reactive Force Field for Molecular Dynamics Simulations of Hydrocarbon Oxidation. The Journal of Physical Chemistry A 112, 1040–1053. https://doi.org/10.1021/jp709896w
"""

from numpy import log, exp

def C2(p,i,j,rij):
    """
    This function ensures the proper destabilisation of C-C triple bond
    Equation 19
    Ref: 
    """
    D_i = None #!
    check = bo(p,i,j,rij) - D_i - 0.04*D_i**4
    if check > 3:
        return p.general["k_c2"]*((check-3)**2)
    elif check <= 3:
        return 0
