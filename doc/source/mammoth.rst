**************
mammoth module
**************

This module provides the base classes for setting up a calculation with Mammoth
and manage mammoth input parameters.

.. automodule:: mammoth

The Mammoth class
-----------------

.. autoclass:: mammoth.Mammoth
    :members:

The MammothControls class
-------------------------

.. autoclass:: mammoth.MammothControls
    :members:
