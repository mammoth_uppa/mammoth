#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Mammoth
-------

Model for Atomic Molecular Mechanics On The Hub

"""

from .mammoth import Mammoth, MammothControls

__author__ = "Hugo Santos-Silva, Germain Vallverdu"
__email__ = "hugo.santos-silva@univ-pau.fr"
__description__ = "Model for Atomic Molecular Mechanics On The Hub"
__version__ = "0.0.1"
