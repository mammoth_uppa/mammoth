# coding: utf-8

"""
Tests for module orca.py
"""

__author__ = "Hugo Santos-Silva, Germain Vallvedu"
__copyright__ = "Copyright 2016, UPPA-CNRS"
__version__ = "0.1"

import os
#import unittest2 as unittest
from pytest import approx
import numpy as np
from pymatgen import Spin

# add above folder to python path in order to run the tests
# useless if you first install orca
import sys
sys.path.insert(0, os.path.abspath('/Users/gvallver/git/mammoth/'))
import orca


test_dir = os.path.join(os.path.dirname(__file__), "../../test_files")


class TestOrcaHessian:

    def setup_method(self):
        self.orca_h = orca.OrcaHessian(os.path.join(test_dir, "nitromethane_orca.hess"))

    def test_molecule(self):
        assert self.orca_h.energy == approx(-244.729446)
        assert len(self.orca_h.molecule) == 7
        assert self.orca_h.molecule.formula == "H3 C1 N1 O2"
        test_coords = np.array(
            [[-2.601369, 0.304535, 0.416229],
             [0.183681, -0.008369, -0.011097],
             [-3.564337, -0.786398, -1.064996],
             [-3.059288, 2.315440, 0.243216],
             [-3.058247, -0.462378, 2.283575],
             [1.202569, 1.515539, -1.400853],
             [1.199553, -1.804041, 1.005993]]).flatten()
        cart_coords = self.orca_h.molecule.cart_coords.flatten()
        assert cart_coords == approx(test_coords)

    def test_frequency(self):
        assert self.orca_h.frequencies[6]["frequency"] == approx(61.56)
        assert self.orca_h.frequencies[20]["frequency"] == approx(3213.89)

        assert self.orca_h.frequencies[6]["IR_intensity"] == approx(0.0281)
        assert self.orca_h.frequencies[20]["IR_intensity"] == approx(1.4025)
        mode = [0.000322, -0.078118, 0.058640, -0.000019, -0.001010, 0.000724,
                -0.004912, -0.014970, -0.000960, -0.147271, 0.682235,
                -0.055811, 0.148181, 0.261218, -0.640130, 0.000211, 0.000580,
                -0.000308, -0.000184, 0.000453, -0.000441]
        assert self.orca_h.frequencies[20]["mode"] == approx(mode)

        col1 = [0.405829,  0.012335,  0.016884, -0.131815,  0.00257,  0.002959,
                -0.100539, -0.068543, -0.092938, -0.057158,  0.062184, -0.003822,
                -0.056949, -0.022109,  0.057732, -0.029815, -0.012729,  0.023713,
                -0.029288,  0.026355, -0.004697]
        assert self.orca_h.hessian[:, 0] == approx(col1)

        last_line = [-4.69700000e-03,   1.19060000e-02,   1.44300000e-02,
                     -8.29420000e-02,   1.70292000e-01,  -1.80180000e-01,
                     3.94900000e-03,  -8.45000000e-04,   1.32000000e-04,
                     -1.60500000e-03,  -5.05000000e-04,   1.59700000e-03,
                     -1.06100000e-03,  -1.03200000e-03,   9.49000000e-04,
                     -1.35890000e-02,   7.44600000e-02,  -2.32830000e-02,
                     1.00121000e-01,  -2.54319000e-01,   1.84778000e-01]
        assert self.orca_h.hessian[-1, :] == approx(last_line)


class TestOrcaVPT2:

    def setup_method(self):
        self.vpt2 = orca.OrcaVPT2(os.path.join(test_dir, "n-CH4.vpt2"))

    def test_molecule(self):
        assert len(self.vpt2.molecule) == 5
        assert self.vpt2.molecule.formula == "H4 C1"
        test_coords = np.array(
            [[0.00012779662, 7.19107e-06, 0.00016394649],
             [2.00126738663, 0.00017835961, 0.48669590152],
             [-0.72738728049, 1.92390901044, 0.08451307148],
             [-1.03191009469, -1.1830970757, 1.33219749509],
             [-0.2434916639, -0.74107591724, -1.9053585509]]).flatten()
        cart_coords = self.vpt2.molecule.cart_coords.flatten()
        assert cart_coords == approx(test_coords)

    def test_hessian(self):
        line1 = [0.540142604690, 0.000134039639, -0.000311533664,
                 -0.314197126270, -0.000117243564, -0.000124152896,
                 -0.075268470824, -0.011267086411, 0.084593308438,
                 -0.075382457625, -0.067399322065, -0.051696431713,
                 -0.075192628677, 0.078615465017, -0.032384908413]
        assert self.vpt2.hessian[0, :] == approx(line1)

        last_line = [-0.032384908413, 0.084851079948, -0.079780704211,
                     0.000124151274, -0.001304990869, -0.001102423037,
                     -0.011430058303,  0.026526215491, -0.009630161539,
                     0.007940983342, -0.016720129033, 0.007557470844,
                     0.035722783864, -0.093286062346, 0.082949005696]
        assert self.vpt2.hessian[-1, :] == approx(last_line)

    def test_dipole_derivative(self):
        assert self.vpt2.dipole_derivative[0, 0] == approx(-0.002604370409)
        assert self.vpt2.dipole_derivative[9, 2] == approx(-0.035702843896)
        assert self.vpt2.dipole_derivative[13, 0] == approx( 0.062599835953)

    def test_cubic(self):
        assert self.vpt2.cubic_terms.shape == (9, 9, 9)
        assert self.vpt2.cubic_terms[0, 0, 0] == approx(46.159000688812)
        assert self.vpt2.cubic_terms[3, 2, 5] == approx(1.813020578534)
        assert self.vpt2.cubic_terms[-1, -1, -1] == approx(0.302323454385)

    def test_semi_quartic(self):
        assert self.vpt2.semi_quartic_terms.shape == (9, 9, 9)
        assert self.vpt2.semi_quartic_terms[0, 0, 0] == approx(-1061585.375845982460)
        assert self.vpt2.semi_quartic_terms[4, 8, 1] == approx(-41864.428167225662)
        assert self.vpt2.semi_quartic_terms[-1, -1, -1] == approx(-2335634.154913417064)


class TestOrcaOutfile:

    def setup_method(self):
        self.oout = orca.OrcaOutfile(os.path.join(test_dir, "nitromethane_orca.out"))
        self.pout = orca.OrcaOutfile(os.path.join(test_dir, "porni_orca.out"))
        self.mout = orca.OrcaOutfile(os.path.join(test_dir, "methanamine_orca.out"))
        self.phosout = orca.OrcaOutfile(os.path.join(test_dir, "phosphine.out"))

    def test_elec_struct(self):
        assert self.pout.is_spin == True
        assert self.pout.charge == 0
        assert self.pout.spin_multiplicity == 1
        assert self.pout.number_electrons == 188
        assert self.pout.number_basis_functions == 976
        assert self.pout.nuclear_repulsion == approx(2579.5279151181)
        assert self.pout.optimization_converged == True
        assert self.pout.normal_termination == True
        assert self.oout.scf_converge == True
        assert self.pout.scf_converge == True
        assert self.mout.scf_converge == True

    def test_dipole(self):
        assert self.pout.dipole_moment == approx([-0.00003, -0.00003, 0.00001])
        assert self.pout.dipole == approx(0.00011)

    def test_energy(self):
        scf = [-2496.607268747929, -2496.607273236285, -2496.607271272344, 
               -2496.607274268820, -2496.607275807946, -2496.607276221700, 
               -2496.607276124478]
        assert self.pout.energies == approx(scf)
        assert self.pout.final_energy == approx(-2496.607276124478)

    def test_bond_order(self):
        # nitromethane
        assert self.oout.bond_orders[(0, 1)] == approx(0.8869)
        assert self.oout.bond_orders[(0, 3)] == approx(0.9657)
        assert self.oout.bond_orders[(1, 5)] == approx(1.5530)
        assert self.oout.bond_orders[(5, 6)] == approx(0.2972)
        # porni
        assert self.pout.bond_orders[(0, 1)] == approx(1.2714)
        assert self.pout.bond_orders[(14, 16)] == approx(1.7252)
        assert self.pout.bond_orders[(14, 30)] == approx(0.9343)
        assert self.pout.bond_orders[(15, 16)] == approx(1.2013)
        assert self.pout.bond_orders[(22, 35)] == approx(0.9344)

    def test_charges(self):
        mul_charges = [0.120719, 0.183039, 0.071971, 0.056293, 0.055725,
                       -0.243975, -0.243771]
        assert self.oout.mul_charges == approx(mul_charges)

        loe_charges = [0.018628, 0.264062, 0.051357, 0.039694, 0.039758,
                       -0.206780, -0.206720]
        assert self.oout.loe_charges == approx(loe_charges)

        assert self.oout.loe_spin_pop == approx(7 * [0.])
        assert self.pout.mul_spin_pop == approx(37 * [0.])

        qh = [-0.021293, -0.232681, 0.095606, 0.095606, 0.028068, 0.011139, 
              0.028068]
        assert self.mout.hirshfeld_charges == approx(qh)

        qesp = [0.303743, -0.916052, 0.340039, 0.340246, 0.000681, -0.068064,
                -0.000593]
        assert self.mout.esp_charges == approx(qesp)

    def test_input(self):
        params = ["pal4", "b3lyp", "d3bj", "tightscf", "opt", "tightopt",
                  "grid5", "uks", "rijcosx", "gridx5", "nofinalgrid", "numfreq",
                  "def2-svp", "def2-svp/c"]
        assert self.oout.orca_input.input_parameters == params
        assert self.pout.orca_input.blocks[0] == \
            '%scf\nmaxiter 1000\nend\n%pal\nnprocs 12\nend\n'

    def test_molecule(self):
        assert len(self.oout.final_structure) == 7
        assert len(self.oout.initial_structure) == 7
        assert len(self.oout.structures) == 13
        assert self.oout.initial_structure.formula == "H3 C1 N1 O2"
        test_coords = np.array(
            [[-1.552770, 1.227774, -0.001943],
             [-0.078985, 1.062193, -0.228074],
             [-2.062351, 0.650478, -0.785774],
             [-1.795091, 2.291899, -0.093497],
             [-1.794540, 0.821942, 0.986214],
             [0.460187, 1.868610, -0.963502],
             [0.458591, 0.111964,  0.310146]]).flatten()
        cart_coords = self.oout.final_structure.cart_coords.flatten()
        assert cart_coords == approx(test_coords)
        symbols = [specie.symbol for specie in self.oout.final_structure.species]
        assert symbols == ["C", "N", "H", "H", "H", "O", "O"]

    def test_thermochemistry(self):
        assert self.pout.temperature == approx(298.15)
        assert self.pout.pressure == approx(1.00)
        assert self.pout.mass == approx(367.04)
        f = self.pout.frequencies
        assert [f[0], f[2], f[-13], f[-1]] == approx(
            [30.96, 108.35, 1703.26, 3244.72])
        data = self.pout.thermo_data
        assert data["zero point energy"][0] == approx(0.27565526)
        assert data["thermal vibrational correction"][0] == approx(0.01420054)
        assert data["thermal rotational correction"][1] == approx(0.89)
        assert data["inner energy"][0] == approx(-2496.31458759)
        assert data["total enthalpy"][0] == approx(-2496.31364338)
        assert data["entropy"][0] == approx(0.05954526)
        assert data["total entropy correction"][1] == approx(-37.37)
        assert self.pout.gibbs_free_enthalpy == approx(-2496.37318864 * 627.503)

    def test_mo(self):
        assert self.phosout.mo_energies[Spin.up][1] == approx(-10.179146)
        assert self.phosout.mo_energies[Spin.up][-2] == approx(2.939872 )
        assert self.phosout.mo_energies[Spin.down][66] == approx(1.315351)
        assert self.phosout.mo_energies[Spin.down][0] == approx(-77.035406)

        assert self.phosout.number_basis_functions == approx(105)
        assert self.phosout.number_electrons == approx(42)

        mo = self.phosout.mo_matrix
        assert mo[Spin.up][5, 5] == approx(-0.931121)
        assert mo[Spin.up][23, 12] == approx(-0.095747)
        assert mo[Spin.up][-1, 35] == approx(0.004972)
        assert mo[Spin.down][10, 4] == approx(0.006177)
        assert mo[Spin.down][33, 20] == approx(0.062460)
        assert mo[Spin.down][-1, -1] == approx(0.120666)

        assert self.phosout.homo[1] == approx(-0.220539)
        assert self.phosout.homo[0] == approx(20)
        assert self.phosout.lumo[1] == approx(0.053565)
        assert self.phosout.lumo[0] == approx(21)


#if __name__ == "__main__":
#    unittest.main()
