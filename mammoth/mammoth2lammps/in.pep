# Solvated 5-mer peptide
#

units		real
atom_style	full

pair_style	lj/charmm/coul/long 8.0 10.0 10.0
bond_style      harmonic
angle_style     charmm
dihedral_style  charmm
improper_style  harmonic
kspace_style	pppm 0.0001

read_data	data.pep

neighbor	2.0 bin
neigh_modify	delay 5

timestep	2.0

thermo_style	multi
thermo		50

fix		1 all nvt temp 275.0 275.0 100.0 tchain 1
fix		2 all shake 0.0001 10 100 b 4 6 8 10 12 14 18 a 31

group		peptide type <= 12

#dump		1 peptide atom 10 dump.peptide

#dump		2 peptide image 25 image.*.jpg type type &
#		axes yes 0.8 0.02 view 60 -30 bond atom 0.5
#dump_modify	2 pad 3

#dump		3 peptide movie 25 movie.mpg type type &
#		axes yes 0.8 0.02 view 60 -30 bond atom 0.5
#dump_modify	3 pad 3

#compute		bnd all property/local btype batom1 batom2
#dump		2 peptide local 300 dump.bond index c_bnd[1] c_bnd[2] c_bnd[3]

dump 1 all custom 1 positions.trj id mass x y z
dump 2 all custom 1 velocities.trj id mass vx vy vz
dump 3 all custom 1 forces.trj id mass fx fy fz
dump 4 all xtc 1 traj.xtc
write_dump all xyz test.xyz modify element C C O H N C C C O H H S O H

run		300

