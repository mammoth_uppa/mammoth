# coding: utf-8

"""
This package contains core modules and classes for representing force fields,
internal coodinates and mammoth parameters.
"""

__author__ = ""
__date__ = ""

from .forcefield import ClassicalForceField
from .molecule import Molecule
from .internal_coords import Bond, Angle, Dihedral, Improper
