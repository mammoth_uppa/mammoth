def get_angles(mol, matrix_cv, cutoffb_inf=2.5):
    """
    Compute force constants for each angle.
    Generation of all possible angles without reversed repetition,
    i.e., (1, 2, 3) will be listed and not (3, 2, 1).

    Args:
        mol: Molecule object (coordinates in angstrom)
        matrix_cv: hessian matrix in kcal.mol^-1.A^-2
        cutoffb_inf (float): cutoff for bonds (default = 2.5 A)

    Returns:
        ang_list: a list for each angle with the index of the three atoms invloved in
                   that angle, its value and the force constant
    """
    nat = len(mol)
    ang_list = []

    for angles in itertools.permutations(range(nat), 3):
        if angles[0] < angles[-1]:

            vv = [mol.cart_coords[angles[1], :] - mol.cart_coords[angles[0], :],
                  mol.cart_coords[angles[1], :] - mol.cart_coords[angles[2], :]]
            rr = [np.linalg.norm(vv[i]) for i in range(len(vv))]
            vvn = [vv[i]/rr[i] for i in range(len(vv))]

            if rr[0] < cutoffb_inf and rr[1] < cutoffb_inf:

                H = [matrix_cv[3 * angles[1]:3 * angles[1] + 3, 3 * angles[2*i]:3 * angles[2*i] + 3] for i in range(2)]
                eigenValues, eigenVectors = np.linalg.eig([H[i] for i in range(len(H))])

                if check_exist(np.concatenate(([eigenValues[i] for i in range(len(H))]))):

                    angle = math.degrees(math.acos((np.dot(vv[0], vv[1])) / (rr[0] * rr[1])))
                    eigvN = [[eigenVectors[i][j]/(np.linalg.norm(eigenVectors[i][j])) for j in range(3)] for i in range(len(H))]

                    un = np.cross(vvn[1], vvn[0]) / (np.linalg.norm(np.cross(vvn[1], vvn[0])))
                    up = [np.cross(un, vvn[0]), np.cross(vvn[1], un)]

                    Kap = [(rr[i] ** 2) * sum([abs(np.dot(up[i], eigvN[i][j]))*eigenValues[i][j] for j in range(3)]) for i in range(2)]
                    Ka = 1/((1/Kap[0]) + (1/Kap[1]))

                    ang_list.append((angles + (angle, (Ka.real)/2)) + (mol[angles[0]].specie, mol[angles[1]].specie, mol[angles[2]].specie))

    return ang_list