***********
core module
***********

.. automodule:: mammoth.core

The core module of Mammoth implements base classes to represent core objects used
in the package such as internal coordinates, molecule or force fields.


Force fields representations
============================

.. automodule:: mammoth.core.forcefield
    :members:

Internal coordinates
====================

.. automodule:: mammoth.core.internal_coords
    :members:

Molecule class
==============

.. automodule:: mammoth.core.molecule
    :members:
