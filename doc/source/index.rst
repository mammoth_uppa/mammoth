.. mammoth documentation master file, created by
   sphinx-quickstart on Fri Jan 27 10:03:58 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

************************
Mammoth's documentation!
************************

Recently, we have found mammoths genetic material preserved in the permafrost
of Siberia. Thanks to genetic engineering, we are able to clone this mammal
eradicated since millions of years and use elephants to bear the baby. Using
the tiny bits of information present in the code, one can recreate the life of
a huge mammal of ancient times.

This is the aim of The Mammoth Project: using bits of information obtained from
*ab initio* calculations of molecules and aggregated systems, we wish to recreate
the "life" of these molecules using Classical Molecular Dynamics. These molecules
can then evolve to others using the basis of Reactive Molecular Dynamics.


What is it and why to use it ?
==============================

Long description with all features ...

* orca module in order to read ORCA output files
* mammoth Modules: `core`, `qm2ff`.
* the mammoth command for a command line interface (CLI)

Licence and contact
===================

Blabla ...

* Hugo Santos Silva hugo.santos-silva@univ-pau.fr
* Germain Vallverdu germain dot vallverdu at univ-pau.fr

Installation
============

Install mammoth from source
---------------------------

In order to install mammoth from source, you must be able to access to the
gitlab repository.

Note that, although not described here, the best practice is to first setup a
pyhon `virtualenv <https://conda.io/docs/user-guide/tasks/manage-environments.html>`_
and then install mammoth in that environment.

1. Clone the repository
^^^^^^^^^^^^^^^^^^^^^^^

Run the following command in order to clone the repository::

    git clone git@gitlab.com:mammoth_uppa/mammoth.git


Take a look at the ``requirements.txt`` to see the mammoth dependencies and
needed packages.

2. Installation
^^^^^^^^^^^^^^^

The installation is supported by setuptools using the file ``setup.py``.
There are two options:

* A classic installation if you only want to use mammoth.
* A developper installation if you want to use and contribute to mammoth.

2.1 Classic installation
""""""""""""""""""""""""

Run the following command::

    python setup.py install --prefix=/path/to/the/install/directory

the `--prefix` option is optional and allows you to install mammoth in a
specific directory.
Note that the installation directory **must be** in your `PYTHONPATH`.
This is a better choice if you want to remove easyly all mammoth files.

For example, if you install mammmoth in `/home/user/mmtest/`, you have first to
update `PYTHONPATH` such as::

    export PYTHONPATH=/home/user/mmtest/lib/python3.6/site-packages:$PYTHONPATH

2.2 Developper installation
"""""""""""""""""""""""""""

To install mammoth as a developper, run the following command::

    python setup.py develop --prefix=/path/to/the/install/directory

again, the ``--prefix`` option allows you to install mammith in a specific directory.
Note that the installation directory **must be** in your ``PYTHONPATH``. See above
for more details.

If you install mammoth as a developper, each change you do in the source code
of mammoth is immediately available. With the basic installation you would have
to install mammoth each time you change something.

3. Uninstall mammoth
^^^^^^^^^^^^^^^^^^^^

If you want to remove all files installed by mammoth you have the following options:

* If you have created a virtualenv, just remove it !
* If you have installed mammoth directly follow the instructions `here <https://stackoverflow.com/a/1550235/1421907>`_


Mammoth command line interface
==============================

The ``mammoth`` command reads the given input file and run the desired
calculations or do the wanted job. run::

    mammoth.py -h

to read the available options.

.. automodule:: mammoth
    :members:

Mammoth library reference
=========================

.. toctree::
    :maxdepth: 2

    mammoth.rst
    core.rst
    qm2ff.rst
    orca.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
