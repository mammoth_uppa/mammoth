************
qm2ff module
************

.. automodule:: mammoth.qm2ff

The qm2ff module mainly implement the `QM2FF` class which implement an automatic
method in order to set up a classical force fields from a QM calculation. The
methodology is based on the following references:

    1. Bautista, E. J.; Seminario, J. M. Int. J. Quantum Chem. 2008, 108 (1), 180–188.
    2. Seminario, J. M. Int. J. Quantum Chem. 1996, 60 (7), 1271–1277.

Summary of implementation strategy
==================================

This is a short summary of the choice made for the implementation.

About bonds
-----------

The core part is the definition of the bonds according to a bond search strategy.

* ``hessian``: bonds are selected if the bond constant is real and positive.
* ``bond_order``: bonds are selected if the bond order value is not null.
* ``hybrid``: union of the ``hessian`` and ``bond_order`` strategy
* ``connectivity``: bonds are selected according to the provide connectivity.

At the end of the bonds definition, the ``bond_list`` list is filled in order to
further build the angle, dihedral and improper lists. The ``bond_list`` is filled
from either the bond_order or the connectivity. That means that the bonds
identified from the force constants computed in the ``hessian`` bond search
strategy are not considered.

About angles
------------

We consider that a bending angle exist between three atoms i, j and k if atom
j is bonded to both atoms i and k.

Next, all force constants are considered and a warning is printed if some
eigenvalues present an imaginary part.

At the end, the ``angle_list`` and the list ``angles`` are consistant.

About dihedrals
---------------

Two types of dihedrals are available. *Parabolic* dihedrals are represented by a
quadratic potential energy such as:

.. math::

    V_{\phi} = k_{\phi}^{para} (\phi - \phi_o)^2

*harminonic* dihedrals are represented by a sinusiidal potential energy such as:

.. math::

    V_{\phi} = k_{\phi}^{harm} \left[1 + \cos(p \phi - \delta) \right]

where $p$ is the periodicity and $\delta$ is the phase of the potential.

The relation between :math:`k_{\phi}^{para}` and :math:`k_{\phi}^{harm}` reads

.. math::

    k_{\phi}^{harm} = \frac{k_{\phi}^{para}}{p^2}

A dihedral angle between atoms i, j, k and l correspond to a torsion aroung the
bond between atoms j and k. That dihedral angle exists if:

* the bonds between i-j, j-k and k-l exist
* if i, j and k are not aligned
* if j, k and l are not aligned

Next, all force constants are considered and a warning is printed if some
eigenvalues present an imaginary part.

Parabolic dihedrals are computed first and harmonic dihedrals, if required, are
computed from the parabolic ones. The periodicity is computed following the
algorithm suggested by Nilson *et al.* from the number of neighbors of atoms j
and k.

* if j and k have got the same number of neighbors

    * number of neighbors is 2, periodicity is 1
    * number of neighbors is 3, periodicity is 2
    * number of neighbors is 4, periodicity is 3

* if j and k have'nt got the same number of neighbors

    * periodicity is 1 if the number of neighbors is 2
    * periodicity is 6 in other cases


If the number of neighbors is larger than 4, until now, an exception is raises.
That case might be encounterd in the case of metallic systems.

At the end, the ``dihedral_list`` and the list ``dihedrals``, are consistant. A
dihedral angle initially in the ``dihedral_list`` is removed if it does not
satisfy the above conditions.

About impropers
===============

We consider that an improper angle exist between atoms i, j, k and l if

* atom i is bonded to atoms j, k and l
* atoms j, k and l are not aligned

Next, all force constants are considered and a warning is printed if some
eigenvalues present an imaginary part.

At the end, the ``improper_list`` and the list ``impropers``, are consistant. An
improper angle initially in the ``improper_list`` is removed if it does not
satisfy the above conditions.

The QM2FF class
===============

.. autoclass:: mammoth.qm2ff.QM2FF
    :members:
