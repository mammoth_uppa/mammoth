#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
This module contains functions to compute the bond order.

Equation numbers are from : van Duin et al. J. Phys. Chem. A 2001

* bo_prime is the function that compute the bond order before correction
* f1, f2, f3, f4 and f5 are functions used to correct the bond order
* bo is the function that compute the corrected bond order.
"""

from numpy import log, exp

def bo(p, i, j, rij):
    """ Compute the corrected bond order, see eq 3a """
    return bo_sigma(p, i, j, rij)+bo_pi(p, i, j, rij)+bo_pipi(p, i, j, rij)

def bo_sigma(p, i, j, rij):
    return bo_prime_sigma(p, i, j,rij)*f1(p, i, j, rij)*f4(p, i, j,rij)*f5(p, i, j, rij)
def bo_pi(p, i, j, rij):
    return bo_prime_pi(p, i, j,rij)*(f1(p, i, j, rij)**2)*f4(p, i, j, rij)*f5(p, i, j, rij)
def bo_pipi(p, i, j, rij):
    return bo_prime_pipi(p, i, j,rij)*(f1(p, i, j, rij)**2)*f4(p, i, j, rij)*f5(p, i, j, rij)

def bo_prime(p,i,j,rij):
    """
    This function compute the bond order BO'_ij see eq 2

    TODO: Check if rij, rij pi and rij pi/pi are all the distance ij or not ?
    """
    return bo_prime_sigma(p, i, j, rij)+bo_prime_pi(p, i, j, rij)+bo_prime_pipi(p, i, j, rij)

def bo_prime_sigma(p, i, j, rij):
    i2=p.atoms[i]['itype'] ##! if not (i,j), then try (j,i), else error print
    j2=p.atoms[j]['itype']
    if i2==j2:
        ro=p.atoms[i]['ro_sigma']
    else:
        ro=p.offdiag[(i2,j2)]['ro_sigma']
    return exp(p.bonds[(i2,j2)]['p_bo1'] * (rij / ro) ** p.bonds[(i2,j2)]['p_bo2'])

def bo_prime_pi(p, i, j, rij):
    i2=p.atoms[i]['itype']
    j2=p.atoms[j]['itype']
    if i2==j2:
        ro=p.atoms[i]['ro_pi']
    else:
        ro=p.offdiag[(i2,j2)]['ro_pi']
    return exp(p.bonds[(i2,j2)]['p_bo3'] * (rij / ro) ** p.bonds[(i2,j2)]['p_bo4'])

def bo_prime_pipi(p, i, j, rij):
    i2=p.atoms[i]['itype']
    j2=p.atoms[j]['itype']
    if i2==j2:
        ro=p.atoms[i]['ro_pipi']
    else:
        ro=p.offdiag[(i2,j2)]['ro_pipi']
    return exp(p.bonds[(i2,j2)]['p_bo5'] * (rij / ro) ** p.bonds[(i2,j2)]['p_bo6'])

def delta_prime(p, i, bo_prime):
    """
    Delta'_i is the degree of deviation of the sum of the uncorrected bond 
    orders around an atomic center from its valency Vali, as described in:

    \Delta'_i = \sum_{j=1}^{nbond} BO'_ij - Val_i

    Args:
        val (int): Val_i: valence of atom i
        bo (list): list of BO'_ij values
    """
    return bo_prime - p.atoms[i]['Val'] ##sum

def delta_prime_boc(p, i, bo_prime):
    
    return bo_prime - p.atoms[i]['Val_boc'] ##sum

def f1(p, i, j, rij):
    """
    Compute function f1, eq 3b
    """
    f2ij = f2(p, i, j, rij)
    f3ij = f3(p, i, j, rij)

    Vali = p.atoms[i]['Val']
    Valj = p.atoms[j]['Val']
    
    return  ((Vali + f2ij) / (Vali + f2ij + f3ij) +  \
             (Valj + f2ij) / (Valj + f2ij + f3ij)) / 2
    
def f2(p, i, j, rij):
    """ compute function f2, eq 3c """
    p_boc1 = p.general['p_boc1']
    delta_prime_i = delta_prime(p, i, bo_prime(p,i,j,rij))
    delta_prime_j = delta_prime(p, j, bo_prime(p,i,j,rij))
    return exp(- p_boc1 * delta_prime_i) + exp( - p_boc1 * delta_prime_j)

def f3(p, i, j, rij):
    """ compute function f3, eq 3d """
    p_boc2 = p.general['p_boc2']
    delta_prime_i = delta_prime(p, i, bo_prime(p,i,j,rij))
    delta_prime_j = delta_prime(p, j, bo_prime(p,i,j,rij))
    return - log( (exp( -p_boc2 * delta_prime_i ) + exp( -p_boc2 * delta_prime_j)) / 2) / p_boc2

def f4(p, i, j, rij):
    """ compute function f4, eq 3e, function f5 is the same, eq 3f """
    p_boc3 = p.atoms[i]['p_boc3']
    p_boc4 = p.atoms[i]['p_boc4']
    p_boc5 = p.atoms[i]['p_boc5']
    bo = bo_prime(p,i,j,rij)
    delta_prime_i = delta_prime_boc(p, i, bo)
    return 1 / (1 + exp(- p_boc3 * (p_boc4 * bo**2 - delta_prime_i) + p_boc5))

def f5(p, i, j, rij):
    """ eq 3f, same as f4 for j """
    p_boc3 = p.atoms[j]['p_boc3']
    p_boc4 = p.atoms[j]['p_boc4']
    p_boc5 = p.atoms[j]['p_boc5']
    bo = bo_prime(p,i,j,rij)
    delta_prime_i = delta_prime_boc(p, j, bo)
    return 1 / (1 + exp(- p_boc3 * (p_boc4 * bo**2 - delta_prime_i) + p_boc5))

#def f5(p, i, j, rij):
#    return f4(p, j, i, rij)
