import re

with open("nitromethane_orca.out", 'r') as f:
    for line in f:
        if "Mayer bond orders larger than 0.1" in line:
            bo_temp = []
            line = f.readline()
            while line != "\n":
                bo_temp.append(line.split("\n"))
                line = f.readline()

bo_temp2 = []
for i in range(len(bo_temp)):
    for n in range(3):
        bo_temp2.append(bo_temp[i][0][4+28*n:14+28*n])
bo_temp2 = [x for x in bo_temp2 if x != '']

bo_pairs = [[] for i in range(len(bo_temp2))]
bo_pairs_atoms = [[] for i in range(len(bo_temp2))]

for i in range(len(bo_pairs)):
    bo_pairs[i] = (int(re.findall(r'\d+', bo_temp2[i])[0]), int(re.findall(r'\d+', bo_temp2[i])[1]))
    bo_pairs_atoms[i] = (re.findall(r'[a-zA-Z]+', bo_temp2[i])[0], re.findall(r'[a-zA-Z]+', bo_temp2[i])[1])

bo_values = []
for i in range(len(bo_temp)):
    for n in range(3):
        bo_values.append(bo_temp[i][0][21+28*n:27+28*n])
bo_values = [x for x in bo_values if x != '']
bo_values = [float(x) for x in bo_values]

for i in range(len(bo_pairs)):
    if bo_values[i] <= 0.5:
        print (bo_pairs[i], bo_pairs_atoms[i], bo_values[i], 'is probably not a real bond')
    elif 0.5 < bo_values[i] <= 1.4:
        print (bo_pairs[i], bo_pairs_atoms[i], bo_values[i], 'is probably a single bond')
    elif 1.4 < bo_values[i] <= 1.6:
        print (bo_pairs[i], bo_pairs_atoms[i], bo_values[i], 'is probably a delocalized double bond')
    elif 1.6 < bo_values[i] <= 2.3:
        print (bo_pairs[i], bo_pairs_atoms[i], bo_values[i], 'is probably a double bond')
    elif 2.3 < bo_values[i] <= 3.3:
        print (bo_pairs[i], bo_pairs_atoms[i], bo_values[i], 'is probably a triple bond')
    else:
        print(bo_pairs[i], bo_pairs_atoms[i], bo_values[i], 'not able to estimate the type of bond')