
# coding: utf-8

import numpy as np
import subprocess


def run_lammps(filename, install_path="/opt/cluster/contrib/apps/lammps/15.10.2015/"):
    lammpslog = subprocess.check_output('lmp < '+filename, shell=True)
    return lammpslog

def read_velocities(timestep, total_time, lammps_traj="vxvyvz.trj"):
    vx = []
    vy = []
    vz = []
    with open(lammps_traj, 'r') as vtrj:
        line = vtrj.readline()
        for line in vtrj:
            data = line.split()
            if data[0] == "#":
                break

    return np.array(vx, vy, vz)



