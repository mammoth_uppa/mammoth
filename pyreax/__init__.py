#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
pyReax
-------

ReaxFF functions

van Duin, A. C. T.; Dasgupta, S.; Lorant, F.; Goddard, W. A. 
ReaxFF: A Reactive Force Field for Hydrocarbons. 
The Journal of Physical Chemistry A 2001, 105 (41), 9396–9409. 
https://doi.org/10.1021/jp004368u.

"""

__author__ = "Germain Salvato Vallverdu"
__email__ = "germain.vallverdu@univ-pau.fr"
__description__ = "Python fonction to compute ReaxFF energy"
__version__ = "0.0.1"