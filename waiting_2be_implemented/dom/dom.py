
# coding: utf-8

# In[ ]:

import numpy as np
import matplotlib.pyplot as plt
get_ipython().magic('matplotlib inline')


# In[ ]:

filename = 'input'

freq = []

with open(filename+".hess", 'r') as f:
    for line in f:
        if '$ir_spectrum' in line:
            ntfreqs = int(f.readline()) 
            for i in range(ntfreqs):
                data = f.readline().split()
                if float(data[0]) != 0.00:
                    freq.append(float(data[0]))

freq = np.array(freq)


# In[ ]:

fwhm = 5.0
resolution = 0.01
lowlim = 0.00
highlim = 4000.01

x = np.arange(lowlim, highlim, resolution)

def lorentz(x, x0, fwhm):
    return (fwhm/2)/(np.pi*((x - x0)**2+(fwhm/2)**2))


def lorentzconv(freq):
    spectre = np.zeros(len(x))
    for f in freq:
        spectre += lorentz(x, f, fwhm)
    return spectre


# In[ ]:

np.savetxt(filename+".spec", np.c_[x, lorentzconv(freq)], fmt="%.5g")


# In[ ]:

plt.title("Densité de modes vibrationnels")
plt.xlabel("Nombre d'onde (cm$^{-1}$)")
plt.ylabel("Intensité (un. arb.)")
plt.plot(x,lorentzconv(freq))
plt.savefig(filename+".png", dpi = 600)
plt.savefig(filename+".pdf", dpi = 600)


# In[ ]:



